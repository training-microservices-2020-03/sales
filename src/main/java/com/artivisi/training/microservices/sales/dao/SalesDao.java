package com.artivisi.training.microservices.sales.dao;

import com.artivisi.training.microservices.sales.entity.Sales;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SalesDao extends PagingAndSortingRepository<Sales, String> {
}

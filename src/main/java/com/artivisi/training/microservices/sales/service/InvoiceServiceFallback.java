package com.artivisi.training.microservices.sales.service;

import com.artivisi.training.microservices.sales.dto.Invoice;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component @Slf4j
public class InvoiceServiceFallback implements InvoiceService {

    @Override
    public void create(Invoice invoice) {
        log.info("Service invoice sedang down, gagal create invoice untuk sales {}",
                invoice.getSalesReference());
    }

    @Override
    public Map<String, Object> invoiceInfo() {
        Map<String, Object> hasil = new HashMap<>();
        hasil.put("address", "0.0.0.0");
        hasil.put("port", -1);
        return hasil;
    }
}

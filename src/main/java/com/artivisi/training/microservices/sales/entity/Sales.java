package com.artivisi.training.microservices.sales.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity @Data
public class Sales {

    @Id @GeneratedValue(generator = "system-uuid2")
    @GenericGenerator(name = "system-uuid2", strategy = "uuid2")
    private String id;
    private LocalDateTime transactionTime;
    private String customerName;
    private String customerEmail;
    private String product;
    private Integer quantity;
    private BigDecimal unitPrice;
}

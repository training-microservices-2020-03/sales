package com.artivisi.training.microservices.sales.controller;

import com.artivisi.training.microservices.sales.dao.SalesDao;
import com.artivisi.training.microservices.sales.dto.Invoice;
import com.artivisi.training.microservices.sales.entity.Sales;
import com.artivisi.training.microservices.sales.service.InvoiceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Map;

@Slf4j
@RestController @RequestMapping("/api/sales")
public class SalesController {

    @Autowired private SalesDao salesDao;
    @Autowired private InvoiceService invoiceService;

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody Sales sales) {
        sales = salesDao.save(sales);

        Invoice invoice = new Invoice();
        invoice.setSalesReference(sales.getId());
        invoice.setAmount(sales.getUnitPrice().multiply(new BigDecimal(sales.getQuantity())));
        invoice.setCustomerName(sales.getCustomerName());
        invoice.setCustomerEmail(sales.getCustomerEmail());
        invoice.setDescription("Sales for " + sales.getCustomerName());
        log.info("Create invoice for sales {}", sales.getId());
        invoiceService.create(invoice);
    }

    @GetMapping("/")
    public Page<Sales> findAll(Pageable pageable) {
        return salesDao.findAll(pageable);
    }

    @GetMapping("/invoice/hostinfo")
    public Map<String, Object> invoiceInfo() {
        log.info("Retrieve info on invoice service");
        return invoiceService.invoiceInfo();
    }
}
